import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import bContainer from 'bootstrap-vue/es/components/layout/container'
import bCard from 'bootstrap-vue/es/components/card/card'
import bCol from 'bootstrap-vue/es/components/layout/col'
import bRow from 'bootstrap-vue/es/components/layout/row'
import bDropdown from 'bootstrap-vue/es/components/dropdown/dropdown'
import bDropdownItem from 'bootstrap-vue/es/components/dropdown/dropdown-item'
import bDropdownDivider from 'bootstrap-vue/es/components/dropdown/dropdown-divider'
import bFormGroup from 'bootstrap-vue/es/components/form-group/form-group'
import bFormCheckboxGroup from 'bootstrap-vue/es/components/form-checkbox/form-checkbox-group'
import VueCharts from 'vue-chartjs'
import { Bar, Line } from 'vue-chartjs'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee, faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faCoffee)
library.add(faCaretUp)
library.add(faCaretDown)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false
Vue.component('b-card', bCard)
Vue.component('b-container', bContainer)
Vue.component('b-row', bRow)
Vue.component('b-col', bCol)
Vue.component('b-dropdown',bDropdown)
Vue.component('b-dropdown-item',bDropdownItem)
Vue.component('b-dropdown-divider',bDropdownDivider)
Vue.component('b-form-group',bFormGroup)
Vue.component('b-form-checkbox-group',bFormCheckboxGroup)



new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

Vue.use(BootstrapVue)
// Vue.component('b-modal', bModal)

