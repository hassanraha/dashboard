let planetChartData = {
    type: 'bar',
    data: {
        datasets: [{
            label: 'Line Dataset',
            data: [10, 50, 30, 50],
            backgroundColor: "",

            // Changes this dataset to become a line
            type: 'line'
        },{
            label: 'Bar Dataset',
            data: [10, 20, 30, 40],
            backgroundColor: ['#04a773', '#04a773', '#04a773', '#04a773']
        }],
        labels: ['January', 'February', 'March', 'April']
    },
    options: {
        responsive: true,
        lineTension: 1,
        scales: {
            yAxes: [{
                ticks: {
                    fontColor: '#FFFFFF',
                    beginAtZero: true,
                    padding: 5,
                }
            }],
            xAxes: [{
                ticks: {
                    fontColor: '#FFFFFF',
                    beginAtZero: true,
                }
            }]
        }
    }
}

export default planetChartData;