let planetChartData = {
    type: 'pie',
    data: {
        datasets: [{
            data: [10, 20, 30],
            backgroundColor: ["#9c332f",'#ffc107','#4a78a8']
        }],
        
        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
            'Red',
            'Yellow',
            'Blue'
        ]
    },
    options: {
        legend: {
            display: false
        }
    }
}

export default planetChartData;